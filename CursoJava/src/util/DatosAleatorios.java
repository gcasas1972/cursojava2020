package util;

import org.apache.poi.util.SystemOutLogger;

public class DatosAleatorios {

	public static float getFloat(int pMinimo, int pMaximo, int pCantDecimales){
		
		double numero = Math.random()*pMaximo;
		while(numero<pMinimo)
			numero = Math.random()*pMaximo;
		int potencia = (int) Math.pow(10, pCantDecimales);
		//System.out.println("potencia=" + potencia);
		int valorfinalint = (int)(numero * potencia);
		float valorFinal= (float)valorfinalint/potencia;
		
		return valorFinal;
	}

}

