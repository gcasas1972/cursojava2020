package util.examples.modelo.test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.asistencia.modelo.Alumno;
import util.AsistenciaUtil;
import util.examples.modelo.Archivo;

public class ArchivoAsistenciaTest {
	String strPath ;
	String strArchivo ;
	Archivo archivo;
	String[] archivos;
	
	List<Alumno> alumnos;
	Alumno alumno, alumno2, alumno3, alumno4;
	
	@Before
	public void setUp() throws Exception {
		strPath = System.getProperty("user.dir") + "/src/util/examples/modelo/test";
		strArchivo ="chat";
		archivo = new Archivo(strPath, strArchivo);
		
		File dir = new File(strPath);
		archivos = dir.list();
		
		alumno = new Alumno(1, "Casas", "Nelson Gabriel");
		alumno2 = new Alumno(7, "Copa", "Matias Federifo");
		alumno3 = new Alumno(9, "COSTANDOPULO", "Leon Guillermo Valenti");
		alumno4 = new Alumno(0, 2, "Perez", "Juan");
		
		
		alumnos = new ArrayList<Alumno>();
		alumnos.add(alumno);
		alumnos.add(alumno2);
		alumnos.add(alumno3);		
		alumnos.add(alumno4);
		
		
	}

	@After
	public void tearDown() throws Exception {
		strPath 	= null;
		strArchivo 	= null;
		archivo 	= null;
		
		archivos 	= null;
		alumno 		= null;
		alumno2		= null;
		alumno3		= null;
		
		alumnos		= null;
	}
	
	@Test
	public void testGetCantidadTotalDeArchivos() {
		assertEquals(3, archivo.getCantidadTotalDeArchivos());
	
	}
	
	@Test
	public void testGetCantidadDePresentes_casas(){
		assertEquals(2, AsistenciaUtil.getCantidadDePresentes(strPath, archivos, "chat", alumno));
	}

	@Test
	public void testGetCantidadDePresentes_copa(){
		assertEquals(1, AsistenciaUtil.getCantidadDePresentes(strPath, archivos, "chat", alumno2));
	}
	
	@Test
	public void testGetCantidadDePresentes_costandopulo(){
		assertEquals(2, AsistenciaUtil.getCantidadDePresentes(strPath, archivos, "chat", alumno3));
	}


	@Test
	public void testGetCantidadDePresentes_0(){
		alumno.setNombres("Roberto Carlos");
		assertEquals(0, AsistenciaUtil.getCantidadDePresentes(strPath, archivos, "chat", alumno));
	}
	@Test
	public void testGetCAntidadDEPresentes_Perez(){
		assertEquals(3, AsistenciaUtil.getCantidadDePresentes(strPath, archivos, "chat", alumno4));
	}

	
}
