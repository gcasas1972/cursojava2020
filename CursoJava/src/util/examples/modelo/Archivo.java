package util.examples.modelo;

import java.io.File;

public class Archivo {
	private String path;
	private String archivoAbuscar;

	
	public Archivo(String path, String pArchivoABusar) {
		super();
		this.path = path;
		this.archivoAbuscar = pArchivoABusar;
	}

	public int getCantidadTotalDeArchivos(){
		int cantidad=0;
		String[] archivos = getCarpetasArchivos(path);
		for (int i = 0; i < archivos.length; i++) {
			if(archivos[i].toUpperCase().contains(archivoAbuscar.toUpperCase()))
				cantidad++;
		}
		return cantidad;
	}
	
	

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getArchivoAbuscar() {
		return archivoAbuscar;
	}

	public void setArchivoAbuscar(String archivo) {
		this.archivoAbuscar = archivo;
	}
	
	private static String[] getCarpetasArchivos(String pPath){
		File dir = new File(pPath);
		String[] ficheros = dir.list();
		return ficheros;
	}
	
}
