package util.examples;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import ar.edu.asistencia.dao.AlumnoDao;
import ar.edu.asistencia.dao.AsistenciaDao;
import ar.edu.asistencia.modelo.Alumno;
import ar.edu.asistencia.modelo.Asistencia;
import ar.edu.asistencia.modelo.Materia;
import util.AlumnoUtil;
import util.ConnectionManager;
import util.DateUtil;
import util.DatosAleatorios;
import util.LeeArchivoUtil;

public class CargaDeDatosBioSystem {

	public CargaDeDatosBioSystem() {	}
	
	public static void main(String[] args) {
		int anio=2021;
		int mes=3;
		for(int dia=1;dia<32;dia++){
			cargaValoresAunParamtroPorDia(1, anio,mes,dia);
		}
		
		
		
	}
	private static void cargaValoresAunParamtroPorDia(int pParametro, int pAnio, int pMes, int pDia ){
		for( int hora =0;hora <24;hora ++){
			for (int minuto=0;minuto <60; minuto +=5){
					
				
				StringBuffer sql = new StringBuffer("insert into valores(par_id, val_fechahora, val_valor, val_unidad) values (");
				Connection con ;
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
				try {
					ConnectionManager.conectar();
					con= ConnectionManager.getConexion();
					
					Statement insert = con.createStatement();
					sql.append(pParametro);
					sql.append(", ");
					
					Date fecha =DateUtil.getDate(pAnio, pMes, pDia, hora, minuto, 00);
					
					sql.append("STR_TO_DATE('");
					sql.append(sdf.format(fecha));
					sql.append("','%d-%m-%Y-%H:%i:%s'),");
					if (pDia<10)
						sql.append(DatosAleatorios.getFloat(20, 30, 2));
					else if (pDia<20)
						sql.append(DatosAleatorios.getFloat(30, 50, 2));
					else 
						sql.append(DatosAleatorios.getFloat(80, 100, 2));
					sql.append(",'�C')");
					insert.executeUpdate(sql.toString());
					
				} catch (ClassNotFoundException | SQLException e1) {			
					e1.printStackTrace();
				}finally{
				try {
					ConnectionManager.desconectar();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				}
			}
		}
				
		}
	}
