package util.examples;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import ar.edu.asistencia.dao.AlumnoDao;
import ar.edu.asistencia.dao.AsistenciaDao;
import ar.edu.asistencia.modelo.Alumno;
import ar.edu.asistencia.modelo.Asistencia;
import ar.edu.asistencia.modelo.Materia;
import util.AlumnoUtil;
import util.AsistenciaUtil;
import util.ConnectionManager;
import util.DateUtil;
import util.LeeArchivoUtil;
import util.examples.modelo.Archivo;

public class CargaDeDatos {

	public CargaDeDatos() {	}
	
	public static void main(String[] args) {
		//1- leo todos los alumnos
		//*********************************************************************************************************
		//String path="C:/gaby/inac/gitlab/2021-videos/compaero7b/AsistenciaElectronica7B.ods";		
//		String path="C:/gaby/inac/gitlab/2021-videos/coumpaernov7a/AsistenciaElectronica7A.ods";
//		String path="C:/gaby/inac/gitlab/2021-videos/electronica6to/AsistenciaElectronica6A.ods";
		//************** se repiten para electronica y tecnicas digitales ******************
//		String path="C:/gaby/inac/gitlab/2021-videos/inac-electronica5a/AsistenciaElectronica5A.ods";		
//		String path="C:/gaby/inac/gitlab/2021-videos/tc4b/AsistenciaTC_4B.ods";

//		String path="C:/gaby/inac/gitlab/2021-videos/td5b/AsistenciaTD_5B.ods";		
//		cargaAlumnosDeOds(path);

		//2- carga alumnos a la materia y las materias son
		//*************************************************************************************************************
// mat_id
//		1, 'Computadoras de Aeronaves', 7, 1
//		2, 'Computadoras de Aeronaves', 7, 2 -
//		3, 'Electr�nica', 6, 1
//		4, 'Electr�nica', 5, 1
//		5, 'T�cnicas digitales', 5, 1
//		6, 'T�cnicas digitales', 5, 2
//		7, 'Teor�a de circuitos', 4, 2
		
//		cargaAlumnosAlaMateria(1, 28, 2);
//		cargaAlumnosAlaMateria(29, 57, 1);	
		//no se inserto el ultimo
//		cargaAlumnosAlaMateria(59, 82, 3);
//		cargaAlumnosAlaMateria(83, 110, 4);
//		cargaAlumnosAlaMateria(83, 110, 5);	
//		cargaAlumnosAlaMateria(111, 139, 7);
//		cargaAlumnosAlaMateria(140, 163, 6);
		
		// 3 cargar asistencia
		//**************************************************************************************************************
		cargaAsistenciaAalumno();
	}
	private static void cargaAsistenciaAalumno(){
		Map<Integer, String> codigosMaterias = new HashMap<>();
		
		codigosMaterias.put(1, "1_coumpaernov_7a");
		codigosMaterias.put(2, "2_compaero_7b");
		codigosMaterias.put(3, "3_electronica_6A");
		codigosMaterias.put(4, "4_sistemasDecontol_6A");		
		codigosMaterias.put(5, "5_electronica_5a");
		codigosMaterias.put(6, "6_tecnicasDigitales_5a");
		codigosMaterias.put(7, "7_tecnicasDigitales_5b");
		codigosMaterias.put(8, "8_teoriaDeCircuitos_4b");
		
		for(int meteria_id =1;meteria_id<=codigosMaterias.size();meteria_id++){
			// Datos de la aistencia
			// 		 1 materia
			//       1 alumno
			//       1 fecha
			// 		 isPresente
			List<Asistencia> asistencias = new ArrayList<>();
			

			//1-	obtengo todos los alumnos de una materia determinada.
			AlumnoDao aluDao = new AlumnoDao();			
			List<Alumno> alumnos = aluDao.leerAlumnosDeMateria(meteria_id);			
			System.out.println("alumnos=" + alumnos );
		    
			
			//2- busco todas las carpetas con las fechas obtengo la fecha de cada una 
	
			String path = "C:/gaby/inac/gitlab/2022-videos/" + codigosMaterias.get(meteria_id);
			System.out.println("la materia que se esta analizando es " + codigosMaterias.get(meteria_id));
			
			// obtengo todas las fecha
			String[] carpetas = getCarpetasArchivos(path);
			
			for (int j = 0; j < carpetas.length; j++) {
				System.out.println();
				System.out.println(carpetas[j]);
				
				//**************** uso de expresiones regulares para saber si es una fecha ********************+
	
				Pattern pat = Pattern.compile("2022.*");			
				Matcher mat = pat.matcher(carpetas[j].trim());
				//esta parte no fue probada, sera probada el dia de hoy
				if (mat.matches() && DateUtil.asNumber(carpetas[j].substring(0, 8))>20220401) {
						// 3- obtengo la fecha						
						String strFecha = carpetas[j].substring(0, 8);
						System.out.println("fecha = " + strFecha);
						Date fecha = DateUtil.asDate(strFecha);
						
						//4- accedo a la carpteta y buso en chatN 
						String pathFecha = path + "/" + carpetas[j];
						
						String[] chats = getCarpetasArchivos(pathFecha);
						
						//5- acceso a los archivos chatN
						//solamente tengo que recorrer los alumnos
						
						Archivo archivo = new Archivo(pathFecha, "chat");
						
						int iCantidadTotalDeArchivos = archivo.getCantidadTotalDeArchivos();
						
						for (Alumno alumno : alumnos) {
							asistencias.add(new Asistencia(	0																		, 
															alumno																	, 
															new Materia(meteria_id, null)											,	 
															fecha																	,
															iCantidadTotalDeArchivos												, 
															AsistenciaUtil.getCantidadDePresentes(pathFecha, chats, "chat", alumno)	, 
															null));
							
						}
						
						
//						for (int contChat=0; contChat<chats.length;contChat++){
//							pat = Pattern.compile("chat.*");
//							mat = pat.matcher(chats[contChat]);
//							if(mat.matches()){
//								//6- abro el archivo y lo leo linea por linea
//								//busco la linea en todos los alumnos 
//								//si esta presente cuento la asistencia la asistencia
//								String pathArchivo = pathFecha + "/" + chats[contChat];
//								List<String> lineas = LeeArchivoUtil.archivoToList(pathArchivo);
//								//************ estos son todos los presentes para  la fecha
//								for (String linea : lineas) {
//									for (Alumno alumno : alumnos) {									
//										if(AlumnoUtil.isPresente(alumno, linea) && !hasAlumnoInAsistencias(alumno, asistencias)){
//											asistencias.add(new Asistencia(0, alumno, new Materia(meteria_id, null), fecha, true, null));
//										}
//									}
//								}															
//								System.out.println("\n\nasistencias presentes cantidad=" + asistencias.size() + "\nDetalle \n" + asistencias);				
//							}							
//						}
						//7 *******luego tengo que generar todos los ausentes despues que recorri todos los chats*****
						
					//	agregarAusentesAsistencia(alumnos, asistencias, new Materia(meteria_id, null), fecha);
						System.out.println("\n\nasistencias presentes y AUSENTNES cantidad=" + asistencias.size() + "\nDetalle \n" + asistencias);

						//8 *********grabar en base de datos
						try {
							AsistenciaDao.agregarAsistenciasAgranel(asistencias);
						} catch (ClassNotFoundException | SQLException e) {									
							e.printStackTrace();
						}
						
						// 9 debo limpiar las asistencias para ir a un nuevo dia
						asistencias = new ArrayList<>();
						
			     } 
	
				
				
		}	
		System.out.println("asistencias: " + asistencias);	
		//3- ingreso a la carpata con la fecah
		//4- leo todos los archivos chat+N
		//5- obtengo los textos 
		//6- Genero una asistencia si el nombre y apellido coinde con el alumno 
		
		}		

		
		
	}
//	private static void agregarAusentesAsistencia(List<Alumno> pAlumnos, List<Asistencia> pAsistencias, Materia pMateria, Date pFecha){
//		for (Alumno alumno : pAlumnos) {
//			if(!hasAlumnoInAsistencias(alumno, pAsistencias))
//				pAsistencias.add(new Asistencia(0, alumno, pMateria, pFecha, false, null));
//		}
//		
//	}
	private static boolean hasAlumnoInAsistencias(Alumno pAlu, List<Asistencia> pAsistencias){
		boolean hasAlumno = false;
		for (Asistencia asistencia : pAsistencias) {
			hasAlumno = asistencia.hasAlumno(pAlu);
			if(hasAlumno)
				break;
		}
		return hasAlumno;
	}

	private static String[] getCarpetasArchivos(String pPath){
		File dir = new File(pPath);
		String[] ficheros = dir.list();
		return ficheros;
	}
	private static void cargaAlumnosAlaMateria(int pCodigoDesd, int pCodigoHasta, int pCodMateria ){
		String sql = new String("insert into materias_alumnos(mat_id, alu_id) values(?,?)");
		Connection con ;
		
		try {
			ConnectionManager.conectar();
			con= ConnectionManager.getConexion();
			
			PreparedStatement insert = con.prepareStatement(sql);
			
			for(int alu_id = pCodigoDesd; alu_id<=pCodigoHasta; alu_id++){
				insert.setInt(1, pCodMateria);
				insert.setInt(2, alu_id);
				insert.executeUpdate();
			}
			
		} catch (ClassNotFoundException | SQLException e1) {			
			e1.printStackTrace();
		}finally{
		try {
			ConnectionManager.desconectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
			
	}

	
	
	
	private static void cargaAlumnosDeOds(String pathOds){
		String sql = new String("insert into alumnos(alu_nrorden, alu_apellido, alu_nombre) values(?,?,?)");

		Connection con ;
	
		
		try {
			ConnectionManager.conectar();
			con= ConnectionManager.getConexion();
			
			List<Alumno> alumnos = getAlumnosDeOds(pathOds);
			System.out.println("alumnos =" + alumnos);
			PreparedStatement insert = con.prepareStatement(sql);
			for (Alumno alumno : alumnos) {
						insert.setInt(1, alumno.getNumeroDeOrden());
						insert.setString(2, alumno.getApellido());
						insert.setString(3, alumno.getNombres());
						insert.executeUpdate();
			}
		//los agrego a la abse de datos
			
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {			
			e.printStackTrace();
		}finally{
			try {
				ConnectionManager.desconectar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
				
	}
	private static List<Alumno> getAlumnosDeOds(String pathArchivo) throws IOException{
		ArrayList<Alumno> alumnos = new ArrayList<>();
		File file = new File(pathArchivo);
		Sheet  sheet = SpreadSheet.createFromFile(file).getSheet(0);
		MutableCell celdaNroOrd, celdaApellidoNombre;
		for(int fila = 3; fila<sheet.getRowCount(); fila++){
			celdaNroOrd = sheet.getCellAt(0,fila);
			celdaApellidoNombre = sheet.getCellAt(1,fila);
			int nroDeOrden=0;
			if( celdaNroOrd.getValue()!=null && !celdaNroOrd.isEmpty()  ){
				nroDeOrden = ((BigDecimal)celdaNroOrd.getValue()).intValue();
				String[] apeYnom = ((String) celdaApellidoNombre.getValue()).split(",");
				alumnos.add(new Alumno(nroDeOrden,apeYnom[0].trim(), apeYnom[1].trim()));
			}
		}
		return alumnos;
	}
	}
