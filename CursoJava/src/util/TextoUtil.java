package util;

public class TextoUtil {

	public static String getTextoSinAcento(String pTexto){
		return pTexto.replace('�', 'a')
					 .replace('�', 'e')
					 .replace('�', 'i')
					 .replace('�', 'o')
					 .replace('�', 'u')
					 ;
	}
	public static String getTextoConvertido(String pTexto){
		return pTexto.replace("á", "a")
					 .replace("é", "e")
					 .replace("í", "i")
					 .replace("ó", "o")
					 .replace("ú", "u")
					 .replace("ñ", "�")
				;
	}
}
