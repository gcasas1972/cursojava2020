package util;

import ar.edu.asistencia.modelo.Alumno;

public class AlumnoUtil {

	/**
	 * Busca dentro de una cadena el nombre y el apellido
	 * @param pAlu Es el alumno a analizar
	 * @param cadena es la cadena donde se va a buscar
	 * @return verdadera si fue encontrado y falso si no lo fue
	 */
	public static boolean isPresente(Alumno pAlu, String pCadena){
		boolean contieneNombre = false;
		boolean contieneApellido = false;
		String [] cadenaArray = pCadena.split(" ");
		int i=0;
		String[] nombres = pAlu.getNombres().split(" ");
		String[] apellidos = pAlu.getApellido().split(" ");
		
		while (!contieneNombre && i<cadenaArray.length){
			if(!cadenaArray[i].isEmpty()){
				int contNom=0;
				while (!contieneNombre && contNom<nombres.length){
					contieneNombre=TextoUtil.getTextoConvertido(cadenaArray[i]).toLowerCase().contains(nombres[contNom].toLowerCase());
					contieneNombre=TextoUtil.getTextoConvertido(pCadena).toLowerCase().contains(nombres[contNom].toLowerCase());
					contNom++;
				}
			}
			i++;
		}
			

		i=0;
		while (!contieneApellido && i<cadenaArray.length){
			if(!cadenaArray[i].isEmpty()){
				int contApe=0;
				while (!contieneApellido && contApe<apellidos.length){
					contieneApellido=TextoUtil.getTextoConvertido(cadenaArray[i]).toLowerCase().contains(apellidos[contApe].toLowerCase());
					contApe++;
				}
			}
			i++;
		}
			

		return contieneNombre & contieneApellido;
	}
	
}
