/**
 * 
 */
package util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Gabriel
 * Esta clase ofrece todos los servicios relacionados con la fecha
 *
 */
/**
 * @author Gabriel
 *
 */
public class DateUtil {
	
	/**
	 * Este m�todo devuelve el a�o de una fecha determinada
	 * @param pDate es la fecha en formato Date
	 * @return devuelve el anio
	 */
	public static int getAnio(Date pDate){
		Calendar cal =Calendar.getInstance();
		cal.setTime(pDate);			
		return cal.get(Calendar.YEAR);
	}
	

	/**
	 * Este metodo devuelve el mes
	 * @param pDate recibe una fecha de tipo date
	 * @return un entero que corresponde al mes
	 */
	public static int getMes(Date pDate){
		Calendar cal =Calendar.getInstance();
		cal.setTime(pDate);			
		return cal.get(Calendar.MONTH)+1;		
	}

	/**
	 * Este metodo reciber una fecha y si es sabado o domingo devuelve verdadero
	 * @param pFecha es la fecha a evaluar
	 * @return verdadero si es fin de semana, caso contrario devuelve falso.
	 */
	public static boolean isFinDeSemana(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		return  cal.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY ||
				cal.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY;
	}
	/**
	 * Este metodo me devuelve el dia de la feecha ingresada por parametro
	 * @param pDate se envia la fecha completa
	 * @return devuelve el dia 
	 */
	public static int getDay(Date pDate){
		Calendar cal =Calendar.getInstance();
		cal.setTime(pDate);			
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	
	/**
	 * Este metodo me devuelve el dia de la feecha ingresada por parametro
	 * @param pDia recibe el dia de la fecha
	 * @param pMes recibe el mes de la fecha
	 * @param pAnio recibe el anio de la fecha
	 * @return devuelve el dia del mes
	 */
	public static int getDay(int pDia, int pMes, int pAnio){
		Calendar cal =Calendar.getInstance();
		cal.set(pAnio,pMes-1, pDia);
		
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * Este m�todo devuelve el ultimo dia del mes, se debe tener en cuenta que la variable cal va a ser modificada
	 * ya que la variable pasa por referencia.
	 * @param cal es la fecha a evaluar
	 * @return es el valor del ultimo dia del mes
	 */
	public static int getUltimoDiaDelMes(Calendar cal){
		int mes = getMes(cal.getTime())-1;
		int iFin = mes+1;
		while(mes<iFin){
			cal.add(Calendar.DAY_OF_MONTH, 1);
			mes = getMes(cal.getTime())-1;
		}
		cal.add(Calendar.DAY_OF_MONTH, -1);
				
		return getDay(cal.getTime());
	}
	
	/**
	 * Esta funcion sirve para mover una variable calendar hasta un dia especificado, se debe tener en cuenta que la variable cal va a ser modificada
	 * ya que la variable pasa por referencia.
	 * @param cal  es la variable a modificar
	 * @param pDayOfWeek corresponde a la constante del dia al que se quiere llegar
	 * @return devuelve una  variable Calentad con la nueva fecha modificada
	 */
	public static Calendar retrocederADia(Calendar cal, int pDayOfWeek){
		cal.add(Calendar.DAY_OF_MONTH, -1);
		while (cal.get(Calendar.DAY_OF_WEEK)!= pDayOfWeek)
			cal.add(Calendar.DAY_OF_MONTH, -1);
		
		return cal;				
	}
	/**
	 * Se le debe ingresar una fecha del formato yyyymmdd y devolver� una fecha
	 * * @param pFecha corresponde a la fecha en formato String
	 * @return devuele 
	 */
	public static Date asDate(String pFecha){
		int anio = Integer.parseInt(pFecha.substring(0, 4));
		int mes  = Integer.parseInt(pFecha.substring(4, 6));
		int dia  = Integer.parseInt(pFecha.substring(6));
		Calendar cal = Calendar.getInstance();
		cal.set(anio, mes-1, dia,00,00,00);
		return cal.getTime();
	}
	/**
	 * convierte una fecha de tipo java.util.Date a java.sql.Date para poder grabar en una base de datos
	 * @param pDate es la fecha que se quiere convertir
	 * @return devuelve una fecha convertida
	 */
	public static java.sql.Date getSqlDate(Date pDate){
	return new java.sql.Date(pDate.getTime());	
	}
	/**
	 * conviete todos los valores enteros en un valor java.sql.Date
	 * @param pAnio a�o
	 * @param pMes valor del mes siendo 1 enero hasta el 12 diciembre
	 * @param pDia dia de la semana
	 * @param pHora hora 0-23
	 * @param pMinuto minutos 0-59 
	 * @param pSegundo segundos 0-59
	 * @return devuelve una fecha de tipo java.sql.date
	 */
	public static java.sql.Date getSqlDate(int pAnio, int pMes, int pDia, int pHora, int pMinuto, int pSegundo){
		SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd-HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.set(pAnio, pMes-1, pDia, pHora,pMinuto, pSegundo);
		System.out.println("fecha=" + sdf.format(cal.getTime()));		
	return new java.sql.Date(cal.getTime().getTime());	
	}
	/**
	 * conviete todos los valores enteros en un valor java.sql.Date
	 * @param pAnio a�o
	 * @param pMes valor del mes siendo 1 enero hasta el 12 diciembre
	 * @param pDia dia de la semana
	 * @param pHora hora 0-23
	 * @param pMinuto minutos 0-59 
	 * @param pSegundo segundos 0-59
	 * @return devuelve una fecha de tipo java.sql.date
	 */

	public static java.sql.Timestamp getSqlTimeStamp(int pAnio, int pMes, int pDia, int pHora, int pMinuto, int pSegundo){
		SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd-HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.set(pAnio, pMes-1, pDia, pHora,pMinuto, pSegundo);
		System.out.println("fecha=" + sdf.format(cal.getTime()));		
	return new java.sql.Timestamp(cal.getTime().getTime());	
	}
	
	/**
	 * conviete todos los valores enteros en un valor Calendar
	 * @param pAnio a�o
	 * @param pMes valor del mes siendo 1 enero hasta el 12 diciembre
	 * @param pDia dia de la semana
	 * @param pHora hora 0-23
	 * @param pMinuto minutos 0-59 
	 * @param pSegundo segundos 0-59
	 * @return devuelve una fecha de tipo Calendar
	 */
	public static Calendar getCalendar(int pAnio, int pMes, int pDia, int pHora, int pMinuto, int pSegundo){
		Calendar cal = Calendar.getInstance();
		cal.set(pAnio, pMes-1, pDia, pHora,pMinuto, pSegundo);
	return cal;	
	}
	/**
	 * Conviete una cadena ordenada la la siguiente forma YYYYMMDD a un valor num�rico
	 * 
	 * @param pFecha es la fecha en formato de String ingresada
	 * @return devuelve un valor numerico entero
	 */
	public static int asNumber(String pFecha){
		
		return Integer.parseInt(pFecha);
	}

/**
 * conviete todos los parametrso enviadso en un objeto de tipo java.util.Date
 * @param pAnio
 * @param pMes
 * @param pDia
 * @param pHora
 * @param pMinuto
 * @param pSegundo
 * @return devuelve un objeto de tipo java.util.Date
 */
	public static Date getDate(int pAnio, int pMes, int pDia, int pHora, int pMinuto, int pSegundo) {
		Calendar cal = Calendar.getInstance();
		cal.set(pAnio, pMes-1, pDia, pHora, pMinuto, pSegundo);
		return cal.getTime();
	}

}
