package util.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import ar.edu.asistencia.modelo.Alumno;
import util.AlumnoUtil;

public class AlumnoUtilTest {
	Alumno alu ;

	@Before
	public void setUp() throws Exception {
		alu = new Alumno(4, "APARILLA", "Soraya Nair");
	}

	@After
	public void tearDown() throws Exception {
		alu = null;
	}

	@Test
	public void testIsPresente() {
		assertTrue(AlumnoUtil.isPresente(alu, "20:23:51	 De  Soraya Amarilla : Soraya Nahir Amarilla"));
	}

}
