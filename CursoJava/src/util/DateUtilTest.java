package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtilTest {

	public static void main(String[] args) {
		Date hoy = new Date();
		
		System.out.println("hoy es el dia " +DateUtil.getDay(hoy));
		System.out.println("hoy es el dia " +DateUtil.getDay(10, 11, 1972));
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 2);
		cal.set(Calendar.MONTH, Calendar.JULY);
		cal.set(Calendar.YEAR, 2020);
		
		cal.add(Calendar.DAY_OF_MONTH, -3);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm EEEEEE" );
		
		System.out.println("hoy es "+ sdf.format(cal.getTime()));
	}

}
