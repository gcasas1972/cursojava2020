package util;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ar.edu.asistencia.modelo.Alumno;
import ar.edu.asistencia.modelo.Asistencia;
import ar.edu.asistencia.modelo.Materia;

public class AsistenciaUtil {

	/**
	 * 
	 * @param pPath
	 * @param pChats
	 * @param patronABuscar
	 * @param pAlu
	 * @return
	 */
	public static int getCantidadDePresentes(String pPath, String[] pChats, String patronABuscar,  Alumno pAlu){
		int cantidad =0;
		for (int contChat=0; contChat<pChats.length;contChat++){
			Pattern pat = Pattern.compile("chat.*");
			Matcher mat = pat.matcher(pChats[contChat]);
			if(mat.matches()){
				//6- abro el archivo y lo leo linea por linea
				//busco la linea en todos los alumnos 
				//si esta presente cuento la asistencia la asistencia
				String pathArchivo = pPath + "/" + pChats[contChat];
				List<String> lineas = LeeArchivoUtil.archivoToList(pathArchivo);
				//************ estos son todos los presentes para  la fecha
				for (String linea : lineas) {												
						if(AlumnoUtil.isPresente(pAlu, linea) ){
							cantidad++;
							break;
						}					
				}															
				
			}							
		}		
		return cantidad;
	}

}