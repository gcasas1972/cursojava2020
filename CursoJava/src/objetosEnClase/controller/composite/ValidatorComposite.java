package objetosEnClase.controller.composite;

import java.util.ArrayList;
import java.util.List;

import objetosEnClase.modelo.Figura;

/**
 * Esta clase agrupa todas las validaciones, es un patron composite por lo cual el padre conoce a todos sus hijos
 * y devuelve la acumulacion de todos los errores
 * @author gcasas
 */
/**
 * @author Maria
 *
 */

public abstract class ValidatorComposite {
	protected static Figura figura;
	
	public ValidatorComposite() {	}
	
	public static String getErrores(Figura fig){
		figura=fig;
		//el padre conoce a todos sus hijos
		List<ValidatorComposite> validaciones = new ArrayList<>();
		//Estas validaciones son de figura
		validaciones.add(new FiguraNombreDobleEspacioComposite());
		validaciones.add(new FiguraNombreVacioComposite());
		//Es de circulo
		validaciones.add(new CirculoRadioNegativoCeroComposite());
		//es de cuadrado
		validaciones.add(new CuadradoLadoNegativoCeroComposite());
		
		StringBuffer sbErrorers = new StringBuffer();
		for (ValidatorComposite validacion : validaciones) {
			if(validacion.isMe() && validacion.validar()){
				sbErrorers.append(validacion.getError());
				sbErrorers.append("\n");
			}
		}
		return sbErrorers.toString();
	}
	
	
	/**
	 * Este metodo devuelve true para el caso de que la validacion corresponda 
	 * @return true si corresponde y false si no corresponde
	 */
	public abstract boolean isMe();
	/**
	 * validar devuelve verdadero para el caso que se de el error 
	 * @return true si se da el error y false si no se da
	 */
	public abstract boolean validar();
	/**
	 * Devuelve el texto del error que le corresponde al validar
	 * @return texto con el error
	 */
	public abstract String getError();
	
	

}
