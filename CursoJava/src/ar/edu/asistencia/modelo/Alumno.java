package ar.edu.asistencia.modelo;

public class Alumno {
	private int codigo			;
	private int numeroDeOrden	;
	private String apellido		;
	private String nombres		;

	public Alumno() {}

	public Alumno(int codigo, int numeroDeOrden, String apellido, String nombres) {
		super();
		this.codigo = codigo;
		this.numeroDeOrden = numeroDeOrden;
		this.apellido = apellido;
		this.nombres = nombres;
	}
	public Alumno( int numeroDeOrden, String apellido, String nombres) {
		super();		
		this.numeroDeOrden = numeroDeOrden;
		this.apellido = apellido;
		this.nombres = nombres;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getNumeroDeOrden() {
		return numeroDeOrden;
	}

	public void setNumeroDeOrden(int numeroDeOrden) {
		this.numeroDeOrden = numeroDeOrden;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	@Override
	public String toString() {
		return "\nAlumno [codigo=" + codigo + ", numeroDeOrden=" + numeroDeOrden + ", apellido=" + apellido + ", nombres="
				+ nombres + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellido == null) ? 0 : apellido.hashCode());
		result = prime * result + ((nombres == null) ? 0 : nombres.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Alumno)) {
			return false;
		}
		Alumno other = (Alumno) obj;
		if (apellido == null) {
			if (other.apellido != null) {
				return false;
			}
		} else if (!apellido.equals(other.apellido)) {
			return false;
		}
		if (nombres == null) {
			if (other.nombres != null) {
				return false;
			}
		} else if (!nombres.equals(other.nombres)) {
			return false;
		}
		return true;
	}
	
	
	

}
