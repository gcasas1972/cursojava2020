package ar.edu.asistencia.modelo;

import java.util.Date;

public class Asistencia {
	private int 	codigo			;
	private Alumno	alumno			;
	private Materia materia			;
	private Date 	fechaHora		;
	private int 	totalSeciones	;
	private int cantidadDePresentes		;
	private String 	observaciones	;
	
	public Asistencia() {}

	public Asistencia(int codigo, Alumno alumno, Materia materia, Date fechaHora, int pCantidadDePresentes,
			String observaciones) {
		super();
		this.codigo = codigo;
		this.alumno = alumno;
		this.materia = materia;
		this.fechaHora = fechaHora;
		this.cantidadDePresentes = pCantidadDePresentes;
		this.observaciones = observaciones;
	}

	
	
	public Asistencia(int codigo, Alumno alumno, Materia materia, Date fechaHora, int totalSeciones, int pCantidadDePresentes,
			String observaciones) {
		super();
		this.codigo = codigo;
		this.alumno = alumno;
		this.materia = materia;
		this.fechaHora = fechaHora;
		this.totalSeciones = totalSeciones;
		this.cantidadDePresentes = pCantidadDePresentes;
		this.observaciones = observaciones;
	}

	public boolean hasAlumno(Alumno pAlu){
		return pAlu.equals(alumno);
	}
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

		
	public int getTotalSeciones() {
		return totalSeciones;
	}

	public void setTotalSeciones(int totalSeciones) {
		this.totalSeciones = totalSeciones;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public int getCantidadDePresentes() {
		return cantidadDePresentes;
	}

	public void setCantidadDePresentes(int pCantidadDePresentes) {
		this.cantidadDePresentes = pCantidadDePresentes;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public String toString() {
		return "\nAsistencia [codigo=" + codigo + ", alumno=" + alumno + ", materia=" + materia + ", fechaHora="
				+ fechaHora + ", isPresente=" + cantidadDePresentes + ", observaciones=" + observaciones + "]";
	}
	
	

	
}
