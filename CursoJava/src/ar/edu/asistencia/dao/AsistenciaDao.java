package ar.edu.asistencia.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import ar.edu.asistencia.modelo.Asistencia;
import util.ConnectionManager;
import util.DateUtil;

public class AsistenciaDao {

	public static void agregarAsistenciasAgranel(List<Asistencia> pAsistencias) throws ClassNotFoundException, SQLException{
		StringBuilder sql = new StringBuilder("insert into lista_de_asistencias(ALU_ID, MAT_ID, ASIS_FECHA, ASIS_TOTALSECIONES, ASIS_CANTPRESENTES, ASIS_OBSERVACIONES)");
														  sql.append(" values(?     , ?		,?			,?	,?			 ,?					)");
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConexion();
		PreparedStatement stm = con.prepareStatement(sql.toString());
		
		for (Asistencia asistencia : pAsistencias) {
			stm.setInt(	1, asistencia.getAlumno().getCodigo());
			stm.setInt(	2, asistencia.getMateria().getCodigo());
			//Date fechaSql = new Date(year, month, day)
			stm.setDate(3,DateUtil.getSqlDate(asistencia.getFechaHora()));
			stm.setInt(4, asistencia.getTotalSeciones());
			stm.setInt(5, asistencia.getCantidadDePresentes());
			stm.setString(6, asistencia.getObservaciones());
			stm.execute();
		
		}
		
	}

}
