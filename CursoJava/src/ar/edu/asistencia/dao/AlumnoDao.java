package ar.edu.asistencia.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.asistencia.modelo.Alumno;
import util.ConnectionManager;

public class AlumnoDao {

	public AlumnoDao() {}

	public List<Alumno> leerAlumnosDeMateria(int pMat_id){
		Connection con;
		Statement stm;
		ResultSet rs;
		List<Alumno> alumnos=null;
		try {
			ConnectionManager.conectar();
			con = ConnectionManager.getConexion();
			StringBuffer sql = new StringBuffer("select mat.mat_id, alu.alu_id, alu_nrorden, alu_apellido, alu_nombre");
									  sql.append(" from alumnos as alu,");
									  sql.append("materias as mat,");
									  sql.append("materias_alumnos as matu");
									  sql.append(" where alu.alu_id = matu.alu_id and");
									  sql.append(" matu.mat_id = mat.mat_id  and");
									  sql.append(" mat.mat_id=");
									  sql.append(pMat_id);
									  
			stm = con.createStatement();
			rs = stm.executeQuery(sql.toString());
			alumnos = getAlumnosFromRs(rs);
			
		} catch (ClassNotFoundException | SQLException e) {
			
			e.printStackTrace();
		}
		return alumnos;
	}
	private List<Alumno> getAlumnosFromRs(ResultSet pRs) throws SQLException{
		List<Alumno> alumnos = new ArrayList<Alumno>();
		while(pRs.next())
			alumnos.add(new Alumno(pRs.getInt("alu_id"),pRs.getInt("alu_nrorden"), pRs.getString("alu_apellido"), pRs.getString("alu_nombre")));
	
		return alumnos;
	}
}
