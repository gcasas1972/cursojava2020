package pantallas.objetos;

public class Jugador extends Persona {
	private String alias;

	public Jugador() {
		super();
	}

	public Jugador(String pNombre, String pApellido) {
		super(pNombre, pApellido);
	}

	public Jugador(String pNombre, String pApellido, String pAlias) {
		super(pNombre, pApellido);
		alias = pAlias;
	}
	public Jugador(String alias) {
		super();
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	public String toString(){
		return super.toString() + ", alias="+ alias;		
	}
	
	
	

}
