-- Ayuda para insertar alumnos en el software -------------------
--1- lista de alumnos por a�o
select  alu_nrorden, alu_apellido, alu_nombre
from alumnos as alu, materias_alumnos mat
where alu.alu_id = mat.alu_id AND
    mat.mat_id = 5
order by alu.alu_nrorden

-- 2- encontrando un grupo de alumnos
SELECT * FROM alumnos
-- where alu_apellido LIKE 'ALLINI%'
WHERE ALU_ID >=215 AND ALU_ID<=244

-- 3- encontrando los alumnos por mat_id
SELECT * FROM materias_alumnos
where mat_id = 8
order by mat_id

-- 4- eliminando a un grupo de alumnos por mat_id
delete from materias_alumnos
where mat_id = 8
----------------------------- fin para ayuda ------------------------------

--agregar alumnos a una materia
-- con cursojava_mayo2022, MateriasAlumnos

-- alumnos a modificar
select * from materias_alumnos
where alu_id between 29 and 58

-- para cuendo tengo 2 materias
	
 update materias_alumnos
 set mat_id = 3
 where alu_id between 83 and 110 and
       mat_id = 4


-- modificar a nueva materia
update materias_alumnos
set mat_id = 9
where alu_id between 29 and 58 and 
      mat_id = 4

-- para verificar el cambio
select  alu.alu_id, alu.alu_nrorden, alu.alu_apellido, alu.alu_nombre, mate.mat_nombre
from alumnos as alu, materias_alumnos as mateAlu, materias as mate,anios as anio
where alu.alu_id     = mateAlu.alu_id and
      mateAlu.mat_id = mate.mat_id    and
	    mate.anio_id   = anio.anio_id   and
      anio.anio_nro  = 2021           and
      mate.div_id    = 2
order by alu.alu_nrorden



-- lista de alumnos por a�o
select  alu.alu_id, alu.alu_nrorden, alu.alu_apellido, alu.alu_nombre, mate.mat_id, mate.mat_nombre
from alumnos as alu, materias_alumnos as mateAlu, materias as mate,anios as anio
where alu.alu_id     = mateAlu.alu_id and
      mateAlu.mat_id = mate.mat_id    and
	    mate.anio_id   = anio.anio_id   and
      anio.anio_nro  = 5              and
      mate.div_id    = 1
order by alu.alu_nrorden

-- lista de alumnos por materia
select alu_nrorden, alu_apellido, alu_nombre 
from alumnos as alu, materias_alumnos mat
where alu.alu_id = mat.alu_id AND
    mat.mat_id = 2
order by alu.alu_nrorden

-- lista de asistencias ordenada por feca
SELECT * FROM lista_de_asistencias
order by asis_fecha

-- 1- Evaluar asistencia cantidad de asistencias alumno, para una materia


SELECT alu.alu_nrorden, alu.alu_apellido, alu_nombre, count(asis_id) 'cantidad de clases', sum(asis_totalseciones) 'seciones', sum(asis_cantpresentes) 'presentes'
FROM   alumnos alu, materias_alumnos matu, lista_de_asistencias asis, materias mat
where  alu.alu_id= matu.alu_id and
	      matu.mat_id= mat.mat_id and
	      alu.alu_id = asis.alu_id and
        asis.asis_fecha BETWEEN '2022-04-01' AND '2022-06-30' and
        mat.mat_id = 1  
group by alu.alu_apellido, alu.alu_nombre
order by alu.alu_nrorden

-- cantidad de presentes por materia para sacar porcentaje de asistencia
select mat.mat_id, mat.mat_nombre, mat.anio_id, mat.div_id, count(asis.asis_id) presentes
from lista_de_asistencias asis, materias mat
where asis.mat_id = mat.mat_id AND
      asis.asis_ispresente=1
group by mat.mat_id

-- para obtener los datos de un alumnos
select alu.alu_nrorden, alu.alu_nombre, alu.alu_apellido, mat.mat_nombre, mat.anio_id, divi.div_letra
from alumnos alu, materias_alumnos matu, materias mat, divisiones divi
where alu.alu_id = matu.alu_id and
      matu.mat_id= mat.mat_id and
      mat.div_id = divi.div_id and
alu.alu_apellido like 'ZOR%'


-- para obtener las asistencias por fecha y materia
SELECT asis.asis_fecha,  mat.mat_nombre, anio.anio_nro, divi.div_letra, alu.alu_nombre, alu_apellido, asis.asis_ispresente
FROM lista_de_asistencias asis, materias mat, alumnos alu, anios anio, divisiones divi
where asis.alu_id = alu.alu_id and
      asis.mat_id = mat.mat_id and
      mat.anio_id = anio.anio_id and
      mat.div_id  = divi.div_id and
	  mat.mat_id = 6  and
	  asis.asis_fecha = '2021-06-04'
order by asis.asis_ispresente, alu.alu_apellido, alu.alu_nombre
	  
	  
-- para obtener las asistencias por fecha por apellido
SELECT asis.asis_id,asis.asis_fecha,  mat.mat_nombre, anio.anio_nro, divi.div_letra, alu.alu_nombre, alu_apellido, asis.asis_ispresente, asis.asis_observaciones
FROM lista_de_asistencias asis, materias mat, alumnos alu, anios anio, divisiones divi
where asis.alu_id = alu.alu_id and
      asis.mat_id = mat.mat_id and
      mat.anio_id = anio.anio_id and
      mat.div_id  = divi.div_id and
--	  asis.asis_ispresente =0 and
--      alu.alu_apellido like 'JUAREZ%'
      asis.asis_id=204  
 
-- actualizacion a presentes
update lista_de_asistencias
set asis_ispresente=1,
        asis_observaciones ='Mirar ninuto 5:42'
where asis_id=204


 
-- cantidad de asistencias por dia , presentes, totales, ausentes
      
SELECT  asis.asis_fecha, asis.mat_id, mat.mat_nombre,mat.anio_id, divi.div_letra, count(asis_id) presentes
FROM lista_de_asistencias asis, materias mat, divisiones divi
where asis.mat_id = mat.mat_id
      and mat.div_id = divi.div_id
     -- and asis_ispresente = 1
group by asis_fecha, mat_id
      


 -- consulta por fecha
 select * FROM lista_de_asistencias
where asis_fecha = STR_TO_DATE('10,3,2021','%d,%m,%Y')

 select * FROM lista_de_asistencias
where asis_fecha = '2021-03-10'
     
      
 -- ************** BASE DE BIOSYSTEM **************     
 -- para cargar temperaturas
  insert into valores (bsp_id, val_fechahora                                          ,val_valor, val_unidad)
         values (  1    ,  STR_TO_DATE('01-03-2021-00:00:00','%d-%m-%Y-%H:%i:%s'), 20.6   , '�c'      )

