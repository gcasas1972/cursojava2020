package objetosPractica.modelo.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import objetosPractica.modelo.Alumno;
import objetosPractica.modelo.Persona;
import objetosPractica.modelo.Profesor;

public class PersonaTest {
	List<Persona> 	personasList	;//1-los elementos se colocan en el mismo orden(ordered), 2- permite duplicados
	Set<Persona>    personasSet		; //1-cualquier orden, 2- NO PERMITE DUPLICADOS
	
	Persona 		persona			;
	Alumno  		alumno			;
	Profesor 		profesor		;

	@Before	
	public void setUp() throws Exception {
		personasList = new ArrayList<Persona>();
		personasList.add(new Alumno("Juan", "Perez", 1010));
		personasList.add(new Profesor("Federico", "Weber", "1510/0"));
		
		personasSet = new HashSet<Persona>();
		personasSet.add(new Alumno("Juan", "Perez", 1010));
		personasSet.add(new Profesor("Federico", "Weber", "1510/0"));
	
	}

	@After
	public void tearDown() throws Exception {
		personasList = null;
		personasSet  = null;
	}
	@Test
	public void testAddList(){
		persona = new Alumno("Juan", "Perez", 1010);
		personasList.add(persona);
		assertEquals(3, personasList.size());
		System.out.println("personasList=" + personasList);
		
		
	}
	@Test
	public void testAddListContiene(){
		persona = new Alumno("Juan", "Perez", 1010);
		
		assertTrue(personasList.contains(persona));
		
	}

	@Test
	public void testAddListNOContiene(){
		persona = new Alumno("Juan", "Perez", 1030);
		
		assertFalse(personasList.contains(persona));
		
	}
	@Test
	public void testAddListSiContienePersona(){
		persona = new Persona("Juan", "Perez");		
		assertTrue(personasList.contains(persona));
		
	}
	@Test
	public void testAddListNOContienePersona(){
		persona = new Persona("Juan", "Alvarerz");		
		assertFalse(personasList.contains(persona));
		
	}
	
	@Test
	public void testAddSet(){
		persona = new Alumno("Juan", "Perez", 1010);
		personasSet.add(persona);
		assertEquals(2, personasList.size());
		System.out.println("personasSet=" + personasList);
		
		
	}
	
	@Test
	public void testPersonaNombre(){
		persona = new Persona("Albert", "Einstein");
		assertEquals("Albert", persona.getNombre());
		
	}
	@Test
	public void testPersonaApellido(){
		persona = new Persona("Albert", "Einstein");
		assertEquals("Einstein", persona.getApellido());
		
	}
	@Test
	public void testPersonaEqualsTRUE(){
		Persona persona1 = new Persona("Albert", "Einstein");
		Persona persona2 = new Persona("Albert", "Einstein");
		
		assertTrue(persona1.equals(persona2));
	}
	
	@Test
	public void testPersonaEqualsFALSE(){
		Persona persona1 = new Persona("Albert", "Einstein");
		Persona persona2 = new Persona("Albert", "Eiin");
		
		assertFalse(persona1.equals(persona2));
	}
	
	@Test
	public void testAlumnoEqualsTRUE(){
		Persona alu1 = new Alumno("Albertito", "Einstein", 1234);
		Persona alu2 = new Alumno("Albertito", "Einstein", 1234);
		
		assertTrue(alu1.equals(alu2));
	}

	@Test
	public void testAlumnoEqualsFALSE(){
		Persona alu1 = new Alumno("Albertito", "Einstein", 1234);
		Persona alu2 = new Alumno("Albertito", "Einstein", 12);
		
		assertFalse(alu1.equals(alu2));
	}

	@Test
	public void testProfesorEqualsTRUE(){
		Persona prof1 = new Profesor("Federico", "Weber", "12345/0");
		Persona prof2 = new Profesor("Federico", "Weber", "12345/0");
		
		assertTrue(prof1.equals(prof2));
		
	}

	@Test
	public void testProfesorEqualsFALSE(){
		Persona prof1 = new Profesor("Federico", "Weber", "12345/0");
		Persona prof2 = new Profesor("Albertito", "Einstein", "12/0");
		
		assertFalse(prof1.equals(prof2));
	}

	
}
