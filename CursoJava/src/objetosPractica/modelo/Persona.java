package objetosPractica.modelo;

public class Persona {
	//atributos
	private String nombre;
	private String apellido;
	
	//constructores
	public Persona() {}
	public Persona(String pNombre, String pApellido) {
		super();
		this.nombre = pNombre;		
		this.apellido = pApellido;
	}

	//getter y setter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String pNombre) {
		this.nombre = pNombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public boolean equals(Object obj){
		boolean bln = false;
		if (obj instanceof Persona){
			//downcast
			Persona per =(Persona)obj;
			bln = per.getNombre()!=null 				&&
				  per.getNombre().equals(nombre) 		&&
				  per.getApellido()!=null 				&&
				  per.getApellido().equals(apellido);				  
		}
		return bln;
	}
	
	public int hashCode(){
		return nombre.hashCode() + apellido.hashCode();
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("\n\nnombre=");
		sb.append(nombre);
		sb.append("\napellido=");
		sb.append(apellido);		
		return sb.toString();
	}
	
	
	

}
