package ar.edu.utn.truco.modelo;

public class Perfil {
	//atributos
	private int codigo;
	
	//constructor tiene el mismo nombre que la clase
	public Perfil(){	}
	
	//getter y setter
	public void setCodigo(int pCod){
		codigo = pCod;		
	}
	
	public int getCodigo(){
		return codigo;
	}
	
}
