package ar.edu.utn.truco.modelo;

public class Pantalla {
	//atributos
	private int codigo;
	
	//constructor tiene el mismo nombre que la clase
	public Pantalla(){	}
	
	//getter y setter
	public void setCodigo(int pCod){
		codigo = pCod;		
	}
	
	public int getCodigo(){
		return codigo;
	}

}
