package ar.com.quidproalg.ws.rest.vo;

import ar.com.quidproalg.modelo.Usuario;

public class VOUsuario {
	//atributos
	private String usuario;
	private String password;
	private boolean isActivo;
	private boolean isValido;
		
	//getter and setter
	public String getUsuario() {					return usuario;				}
	public void setUsuario(String usuario) {		this.usuario = usuario;		}	
	
	public String getPassword() {		return password;						}
	public void setPassword(String password) {		this.password = password;	}
	
	public boolean isValido() {		return isValido;							}	
	public void setValido(boolean isValido) {		this.isValido = isValido;	}
	
	public boolean isActivo() {		return isActivo;							}
	public void setActivo(boolean isActivo) {		this.isActivo = isActivo;	}
	
	//negocio
	

	
	

}
