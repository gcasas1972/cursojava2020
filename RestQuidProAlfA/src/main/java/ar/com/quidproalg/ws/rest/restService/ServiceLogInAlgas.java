package ar.com.quidproalg.ws.rest.restService;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ar.com.quidproalg.controller.TextoController;
import ar.com.quidproalg.controller.UsuarioController;
import ar.com.quidproalg.ws.rest.vo.VOTexto;
import ar.com.quidproalg.ws.rest.vo.VOUsuario;

@Path("/Algas")
public class ServiceLogInAlgas {
	@POST
	@Path("/validaUsuario")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public VOUsuario validaUsuario(VOUsuario pVo){
		UsuarioController usuarioController = new UsuarioController(pVo);
		
		return (VOUsuario)usuarioController.leerHandler();
	}
	
	@POST
	@Path("/traducir")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public VOTexto traducir(VOTexto pText){
		TextoController textController = new TextoController(pText);		
		return (VOTexto)textController.leerHandler();
	}

}