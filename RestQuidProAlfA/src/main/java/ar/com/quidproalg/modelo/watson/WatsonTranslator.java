package ar.com.quidproalg.modelo.watson;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;

import ar.com.quidproalg.modelo.TextoAevaluar;

public class WatsonTranslator {
	private TextoAevaluar textoAevaluar;
	public WatsonTranslator() {	}

	//constructores
	public WatsonTranslator(String textoOriginal, String textoTraducido, String status, String origenDestino) {
		super();
		textoAevaluar = new TextoAevaluar(textoOriginal, textoTraducido, status, origenDestino);
	}
	

	public WatsonTranslator(TextoAevaluar textoAevaluar) {
		this.textoAevaluar = textoAevaluar;
	}

	
	public TextoAevaluar getTextoAevaluar() {		return textoAevaluar;			}
	public void setTextoAevaluar(TextoAevaluar pText) {this.textoAevaluar = pText;	}

	//metodo de negocio
	public void ejecutar(){
	    Authenticator authenticator = new IamAuthenticator("k9u-JBSZzx-ds0CdoZKS7yrRX1juig5MQsaK98663ogm");
	    LanguageTranslator service = new LanguageTranslator("2018-05-01", authenticator);

	    TranslateOptions translateOptions = new TranslateOptions.Builder()
	        .addText(textoAevaluar.getTextoOriginal())
	        .modelId((textoAevaluar.getOrigenDestino()==null || 
	        		textoAevaluar.getOrigenDestino().isEmpty()
	        			)?"en-es":textoAevaluar.getOrigenDestino())
	        .build();
	    TranslationResult translationResult = service.translate(translateOptions).execute().getResult();
	    textoAevaluar.setTextoTraducido(translationResult.toString());
	    textoAevaluar.setStatus("OK");
	}
	
 

}
