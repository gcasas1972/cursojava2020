package ar.com.quidproalg.modelo;

import ar.com.quidproalg.ws.rest.vo.VOUsuario;

public class Usuario implements Vaciable{
	//atributos
	int codigo;
	String usuario;
	String clave ;
	boolean activo;
	// constructores
	public Usuario() {	}
	public Usuario(int pCod){
		codigo = pCod;
	}
	
	public Usuario(int pCod, String pUsu, String pClave, boolean pActivo) {
		this.codigo = pCod;
		this.usuario = pUsu;
		this.clave = pClave;
		this.activo = pActivo;		
	}

	//getter y settter

	public Usuario(String pUsu, String pClave, boolean pActivo) {
		usuario = pUsu;
		clave = pClave;
		activo = pActivo;
	}
	public Usuario(String pUsuario){
		usuario = pUsuario;
	}
	public Usuario(VOUsuario pVo) {
		this.usuario = pVo.getUsuario();
		this.clave = pVo.getPassword();
		this.activo = pVo.isActivo();
		
	}
	public int getCodigo() {return codigo;	}
	public void setCodigo(int pCod) {		this.codigo = pCod;	}
	
	public String getUsuario() {return usuario;	}
	public void setUsuario(String pUsu) {usuario = pUsu;	}

	public String getClave() {return clave;	}
	public void setClave(String pClave) {this.clave = pClave;	}

	public boolean isActivo() {	return activo;	}
	public void setActivo(boolean pAct) {	this.activo = pAct;	}

// de negocio
	
	public boolean isVacio() {
		return codigo==0 										&&
				(usuario==null 		||	usuario.isEmpty())		&&
				(clave ==null 		|| 	clave.isEmpty())		;  
				
	}

	public void vaciar() {
		codigo = 0;
		usuario = null;
		clave = null;
		activo = false;
		
	}
 public void validar(VOUsuario pVoUsu){
	Usuario usu = new Usuario(pVoUsu);
	pVoUsu.setActivo(this.activo);	
	pVoUsu.setValido(usu.equals(this));
								
}

	public boolean equals(Object obj){
		
		return obj instanceof Usuario						&&
				//verifico que sean iguales los  null
				(
				usuario==null 								&& 
				((Usuario)obj).getUsuario() == null 		&&
				clave== null 								&& 
				((Usuario)obj).getClave() ==null 
				)
				||
				//verifico que sean distintos de null para comparar
				(
				usuario!=null 			  					&&
				usuario.equals(((Usuario)obj).getUsuario())	&&
				clave !=null 								&&
				clave.equals(((Usuario)obj).getClave())
				);
	}
	public int hashCode(){
		int num ;
		
		return usuario!=null?usuario.hashCode():0 +
			   clave !=null?clave.hashCode():0	; 
	}
	public String toString(){
		StringBuffer sb = new StringBuffer("usurio=");
		sb.append(this.usuario) ;
		sb.append(",clave=");
		sb.append(this.clave);
		sb.append(",activo=");
		sb.append(this.activo);
		return sb.toString();
	}

}
