package ar.com.quidproalg.modelo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.com.quidproalg.modelo.Usuario;
import ar.com.quidproalg.util.ConnectionManager;




public class UsuarioDao implements DAO {

	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		
		//tengo que castaear a usuario
		Usuario usu = (Usuario)obj;
		
		StringBuffer sql = new StringBuffer("insert into usuarios (usu_usuario, usu_clave, usu_activo) values ('");
		sql.append(usu.getUsuario());
		sql.append("','");
		sql.append(usu.getClave());
		sql.append("',");
		sql.append(usu.isActivo());		
		sql.append(")");
		
		agregar.executeUpdate(sql.toString())	;
		
	}

	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		
		//tengo que castaear a curso
		Usuario usu = (Usuario)obj;
		
		StringBuffer sql = new StringBuffer("delete from usuarios where usu_id=");
		sql.append(usu.getCodigo());
		
		eliminar.executeUpdate(sql.toString())	;

	}

	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		
		//tengo que castaear a curso
		Usuario usu = (Usuario)obj;
		
		StringBuffer sql = new StringBuffer("update usuarios set usu_usuario='");
		sql.append(usu.getUsuario());

		sql.append("',usu_clave='");
		sql.append(usu.getClave());
		
		sql.append("',usu_activo=");
		sql.append(usu.isActivo());

		sql.append(" where usu_id =");
		sql.append(usu.getCodigo());
		
		modificar.executeUpdate(sql.toString())	;
	}


	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		Usuario usu = (Usuario)obj;
		
		StringBuffer sql = new StringBuffer("select usu_id, usu_usuario, usu_clave, usu_activo from usuarios");
		if (usu!=null && !usu.isVacio()){
			if (usu.getCodigo()>0){
				sql.append(" where usu_id=");
				sql.append(usu.getCodigo());
			}else if (usu.getUsuario()!=null && !usu.getUsuario().isEmpty()){
				sql.append(" where usu_usuario  like '%");
				sql.append(usu.getUsuario());
				sql.append("%'");
			}else if (usu.isActivo()== true){
				sql.append(" where usu_activo =");
				sql.append(usu.isActivo());
					
			}
		}
		sql.append(" order by usu_usuario, usu_clave");
		ResultSet rs = leer.executeQuery(sql.toString());
		List cursos = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return cursos;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		while (rs.next()){
			usuarios.add(new Usuario(rs.getInt("usu_id"),rs.getString("usu_usuario"), rs.getString("usu_clave"), rs.getBoolean("usu_activo")));			
		}
		return usuarios;
	}
	

}
