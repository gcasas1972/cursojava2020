package ar.com.quidproalg.modelo.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.quidproalg.modelo.Usuario;
import ar.com.quidproalg.modelo.dao.UsuarioDao;
import ar.com.quidproalg.util.ConnectionManager;



public class UsuarioDaoTest {
	static Connection con;
	Statement stat;
	Usuario usu = null;
@BeforeClass	
public static void setUpBeforeClass() throws Exception {
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	
    Statement consulta= con.createStatement();

    String sql = "";
    BufferedReader bf = new BufferedReader( new InputStreamReader( UsuarioDaoTest.class.getResource( "UsuariosCrear.sql" ).openStream() ) );
    while ( (sql = bf.readLine()) != null ) {
       if ( sql.trim().length() != 0 &&
            !sql.startsWith( "--" ) ) {              
          consulta.executeUpdate( sql ); // aca arma
       }
    }
    cm.desconectar();
}

@AfterClass
public static void tearDownAfterClass() throws Exception {		
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	
    Statement consulta= con.createStatement();

    String sql = "";
    BufferedReader bf = new BufferedReader( new InputStreamReader( UsuarioDaoTest.class.getResource( "UsuariosEliminar.sql" ).openStream() ) );
    while ( (sql = bf.readLine()) != null ) {
       if ( sql.trim().length() != 0 &&
            !sql.startsWith( "--" ) ) {              
          consulta.executeUpdate( sql ); // aca arma
       }
    }
    
    cm.desconectar();
}


@Before
public void setUp() throws Exception {
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	stat = con.createStatement();

}

@After
public void tearDown() throws Exception {
	con.close();
}

@Test
public void testAgregar() {
	UsuarioDao usuDao = new UsuarioDao();
	try {
		//agregque una provincia
		usuDao.agregar(new Usuario("nuevo usuario_test", "claveNueva_test", true));
		//hago una consulta
		StringBuffer sql = new StringBuffer("select usu_id, usu_usuario, usu_clave, usu_activo");
		sql.append(" from usuarios");
		sql.append(" where usu_usuario=");
		sql.append("'nuevo usuario_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();
		assertEquals(rs.getString("usu_usuario"), "nuevo usuario_test");
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testEliminar() {
	UsuarioDao usuDao = new UsuarioDao();
	try {
				
		//hago una consulta buscando el curso eliminar
		StringBuffer sql = new StringBuffer("select usu_id, usu_usuario, usu_clave, usu_activo");
		sql.append(" from usuarios");
		sql.append(" where usu_usuario=");
		sql.append("'usuarios1_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Usuario cur = new Usuario(rs.getInt("usu_id"),rs.getString("usu_usuario"),rs.getString("usu_usuario"), rs.getBoolean("usu_activo"));			
		usuDao.eliminar(cur);

		ResultSet rs2=stat.executeQuery(sql.toString());
		//next deberia darle false, o sea no hay un proximo
		assertFalse(rs2.next());
		rs.close();
		rs2.close();			
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testModificar() {
	UsuarioDao usuDao = new UsuarioDao();
	try {
				
		//hago una consulta buscando el curso paraa modificar
		
		StringBuffer sql = new StringBuffer("select usu_id, usu_usuario, usu_clave,  usu_activo");
		sql.append(" from usuarios");
		sql.append(" where usu_usuario=");
		sql.append("'usuarios2_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Usuario usu = new Usuario(rs.getInt("usu_id"),rs.getString("usu_usuario"),  rs.getString("usu_clave"), rs.getBoolean("usu_activo"));	
		usu.setUsuario("usuarios2_modificado_test");
		usuDao.modificar(usu);

		StringBuffer sql2 = new StringBuffer("select usu_id, usu_usuario, usu_clave,  usu_activo");
		sql2.append(" from usuarios");
		sql2.append(" where usu_usuario=");
		sql2.append("'usuarios2_modificado_test'");
		
	
		ResultSet rs2=stat.executeQuery(sql2.toString());
		rs2.next();
		//next deberia darle false, o sea no hay un proximo
		assertEquals(rs2.getString("usu_usuario"), "usuarios2_modificado_test");
		rs.close();
		rs2.close();			
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testLeer_porUsuario() {
	UsuarioDao usuDao = new UsuarioDao();
	try {
		//leer un curso
		List usuarios =usuDao.leer(new Usuario("usuarios4_test"));
		//hago una consulta
		Usuario usu = (Usuario)usuarios.get(0);
		assertEquals(usu.getUsuario(), "usuarios4_test");
		//assertEquals(cur.getAnio(), 3);
		
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}
@Test
public void testLeer_porCodigo() {
	UsuarioDao usuDao = new UsuarioDao();
	try {

		//hago una consulta buscando el curso para leer
		
		StringBuffer sql = new StringBuffer("select usu_id, usu_usuario, usu_clave,  usu_activo");
		sql.append(" from usuarios");
		sql.append(" where usu_usuario=");
		sql.append("'usuarios3_test'");

		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Usuario usu = new Usuario(rs.getInt("usu_id"));	

		//leer un curso
		List usuarios =usuDao.leer(usu);
		//hago una consulta
		Usuario usuLeido = (Usuario)usuarios.get(0);
		assertEquals(usuLeido.getUsuario(), "usuarios3_test");
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

}
