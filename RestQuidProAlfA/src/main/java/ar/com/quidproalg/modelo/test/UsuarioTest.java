package ar.com.quidproalg.modelo.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.com.quidproalg.modelo.Usuario;

public class UsuarioTest {
	Usuario usuVacio, usuLLeno;	

	@Before
	public void setUp(){
		usuVacio = new Usuario();
		usuLLeno = new Usuario(1, "usuario", "clave", true);
	}
	
	@After
	public void tearDown() throws Exception {
		usuVacio=null;
	}
	@Test
	public void testIsVacio() {
		assertTrue(usuVacio.isVacio());
	}

	@Test
	public void testVaciar() {
		usuLLeno.vaciar();
		
		assertTrue(usuLLeno.isVacio());
	}

	@Test
	public void testEquaqlsConDatos_true() {
		usuVacio.setCodigo(1);
		usuVacio.setUsuario("usuario");
		usuVacio.setClave("clave");
		usuVacio.setActivo(true);
		assertTrue(usuVacio.equals(usuLLeno));
	}
	@Test
	public void testEquaqlsConDatos_False() {
		usuVacio.setCodigo(1);
		usuVacio.setUsuario("usuario2");
		usuVacio.setClave("clave");
		usuVacio.setActivo(true);
		assertFalse(usuVacio.equals(usuLLeno));
	}
	@Test
	public void testEquaqlsConDatos2_False() {
		usuVacio.setCodigo(1);
		usuVacio.setUsuario(null);
		usuVacio.setClave("clave");
		usuVacio.setActivo(true);
		assertFalse(usuVacio.equals(usuLLeno));
	}
	
	@Test
	public void testEquaqlsConDatosnull_true() {
		usuLLeno.vaciar();
		assertTrue(usuLLeno.equals(usuVacio));
	}
	
	@Test
	public void testEquaqlsConCodigo_true() {
		usuLLeno.vaciar();
		usuLLeno.setCodigo(45);
		assertTrue(usuLLeno.equals(usuVacio));
	}
}
