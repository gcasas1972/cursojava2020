package ar.com.quidproalg.modelo.test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

import ar.com.quidproalg.ws.rest.vo.VOUsuario;

public class RestClientTest {

	public static void main(String[] args) {
		VOUsuario voUsu = new VOUsuario();
		voUsu.setUsuario("gcasas");
		voUsu.setPassword("alquid");
		voUsu.setActivo(false);
		voUsu.setValido(false);
		
		
		ClientConfig  clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		
		Client client = Client.create(clientConfig);
		WebResource webResource = 
							client.resource("http://inac.hopto.org:8080/RestQuidProAlfA/services/Algas/validaUsuario");
		ClientResponse response = webResource.type("application/json")
				.post(ClientResponse.class, voUsu);
		voUsu = response.getEntity(VOUsuario.class);
		System.out.println("usuario:" + voUsu.getUsuario());
		System.out.println("clave=" + voUsu.getPassword());
		System.out.println("isValdo=" + voUsu.isActivo());
		System.out.println("isActivo=" + voUsu.isActivo());
		
		
	}

}
