package ar.com.quidproalg.modelo;

import ar.com.quidproalg.ws.rest.vo.VOTexto;

public class TextoAevaluar implements Vaciable {
	private String textoOriginal;
	private String textoTraducido;
	private String status;
	private String origenDestino ;

	//constructores
	
	public TextoAevaluar() {}
	

	public TextoAevaluar(String textoOriginal, String textoTraducido, String status, String pOrigenDest) {
		super();
		this.textoOriginal = textoOriginal;
		this.textoTraducido = textoTraducido;
		this.status = status;
		this.origenDestino = pOrigenDest;
	}
	public TextoAevaluar(VOTexto pvoText) {
		this.textoOriginal 	= pvoText.getTextoOriginal();
		this.textoTraducido	= pvoText.getTextoTraducido();
		this.origenDestino  = pvoText.getOrigenDestino();
		this.status		  	= "en proceso";
		
	}


	// getter y setter
	public String getTextoOriginal() {		return textoOriginal;															}
	public void setTextoOriginal(String textoOriginal) {		this.textoOriginal = textoOriginal;							}

	public String getTextoTraducido() {		return textoTraducido;															}
	public void setTextoTraducido(String textoTraducido) {		this.textoTraducido = getTextoConvetido(textoTraducido);	}

	public String getStatus() {		return status;																			}
	public void setStatus(String status) {		this.status = status;														}
	
	public String getOrigenDestino() {		return origenDestino;															}
	public void setOrigenDestino(String origenDestino) {		this.origenDestino = origenDestino;							}


	private String  getTextoConvetido (String ptext){
		int inicio 	= ptext.indexOf("translations");
		inicio 		+=  45;
		int fin 	= ptext.indexOf("\n", inicio +1)-1;
		String miTexto = ptext.substring(inicio, fin);
		return miTexto;
		
	}
	//metodos de negocio
	public void vaciar() {
		textoOriginal	=null;
		textoTraducido	=null;
		status 			=null;
	}



	public boolean isVacio() {
		return textoOriginal!=null && 
			   !textoOriginal.isEmpty() &&
			   
			   textoTraducido!=null &&
			   !textoTraducido.isEmpty() &&
			   
			   status!=null &&
			   !status.isEmpty();
	}
	
	public boolean equals(Object obj){
		return obj 	instanceof TextoAevaluar &&
				!isVacio()					&&
				((TextoAevaluar)obj).getTextoOriginal().equals(textoOriginal) 	&&
				((TextoAevaluar)obj).getTextoTraducido().equals(textoTraducido) &&
				((TextoAevaluar)obj).getStatus().equals(status) 				;
	}
	public int hashCode(){
		return isVacio()?0:	textoOriginal.hashCode() +
							textoTraducido.hashCode() +
							status.hashCode();
	}
	public String toString(){
		StringBuffer sb = new StringBuffer("textoOriginal=");
		sb.append(textoOriginal);
		sb.append(",textoTraducido=");
		sb.append(textoTraducido);
		sb.append(",status=");
		sb.append(status);
		
		return sb.toString();
	}

}
