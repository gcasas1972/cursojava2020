package ar.com.quidproalg.controller;

import java.sql.SQLException;
import java.util.List;

import ar.com.quidproalg.modelo.Usuario;
import ar.com.quidproalg.modelo.dao.UsuarioDao;
import ar.com.quidproalg.ws.rest.vo.VOUsuario;

public class UsuarioController implements Controller {
	Usuario usuario;
	VOUsuario voUsuario;
	public UsuarioController(VOUsuario pVo) {
		voUsuario = pVo;
		usuario = new Usuario(pVo);		
	}

	public Object leerHandler() {
		UsuarioDao usuDao = new UsuarioDao();
		List<Usuario> usuarios;
		Usuario usuLeido;
		
		try {
			//se lee de la base de datso
			usuarios = usuDao.leer(usuario);
			usuLeido = usuarios!=null && !usuarios.isEmpty() ? usuarios.get(0):new Usuario();
			//usuario leido de la base de datos hay que compararlo con el 
			usuLeido.validar(voUsuario);
			
			
		} catch (ClassNotFoundException e) {
			//negociar como devolver el error
			e.printStackTrace();
		} catch (SQLException e) {			
			e.printStackTrace();
		} catch(Exception e){
			e.printStackTrace();
		}
		
		

		return voUsuario;
	}

}
