package ar.com.quidproalg.controller;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;

import ar.com.quidproalg.modelo.TextoAevaluar;
import ar.com.quidproalg.modelo.watson.WatsonTranslator;
import ar.com.quidproalg.ws.rest.vo.VOTexto;

public class TextoController implements Controller {
	private TextoAevaluar textoAevaluar	;
	private VOTexto		 voTexto		;
	
	public TextoController(VOTexto pvoText) {
		
		this.voTexto = pvoText;
		textoAevaluar = new TextoAevaluar(pvoText);
	}


	public Object leerHandler() {
		
		WatsonTranslator wt = new WatsonTranslator(	textoAevaluar.getTextoOriginal()	,
													textoAevaluar.getTextoTraducido()	,
													textoAevaluar.getStatus()			, 
													textoAevaluar.getOrigenDestino());
		wt.ejecutar();
		voTexto.setTextoTraducido(wt.getTextoAevaluar().getTextoTraducido());
		voTexto.setStatus(wt.getTextoAevaluar().getStatus());
		return voTexto;
	}

}
